@extends('layouts.template')
@section('content')
<script>
    //jquery object to access rows.
    $(document).ready(function(){    $('.dataTable').DataTable();});
</script>

<div class="page-header" >
    <h1>List of films</h1>
</div>
    @if ($movies->isEmpty())
        no movies
    @else
<table class="dataTable">
<thead>
<tr>
<th>Title</th>
<th>description</th>
</tr>
</thead>
<tbody>

         @foreach($movies as $movie)
    <tr>
    <td>{{ $movie->title }}</td>
    <td>{{ $movie->description }}</td>


<td>
    <a href="{{ url('booking/list/'.$movie->movie_id) }}" class="btn btn-danger">continue</a>
</td>
    </tr>
                @endforeach
            </tbody>
</table>
    @endif
@stop