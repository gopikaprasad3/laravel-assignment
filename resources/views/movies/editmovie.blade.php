@extends('layouts.template')
<h1>EDIT MOVIE</h1>
<div class="cinemaHall">
    <?php 
    // dd($booking);
       use App\Models\Movie;
     ?>
    
    <form id="movie" method="post" action="{{ url('movies/editMovie') }}">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
      
        <label for ='title'>Enter tilte</label>
        <input type="text"  name=title>

        <label for ='description'>Enter description</label>
        <input type="text"  name=description>

        <label for ='seats_available'>Enter available seats</label>
        <input type="text"  name=seats_available>


        <label for ='show_time'>Enter show time</label>
        <input type="text"  name=show_time>
        <button type="submit" class="btn btn-danger">Edit</button>

    </form>

</div>