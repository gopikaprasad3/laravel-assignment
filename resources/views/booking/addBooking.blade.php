@extends('layouts.template')
<h1>Seat Booking</h1>
<div class="cinemaHall">
    <?php 
    // dd($booking);
       use App\Models\Movie;
    //    dd($movie);
     ?>
    
    <form id="booking" method="post" action="{{ url('booking/addBooking') }}">

    <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <label for ='available_seats'>available seats</label>
        <input type="text" value="{{$movie->seats_available}}"readonly><br>
        <input type="text" value="{{$movie->movie_id}}" name="movie_id" hidden>
        <label for ='seats_number'> Seats::</label>
        <input type="number" max="{{$movie->seats_available}}" min=1 name=seats_number>
        <button type="submit" class="btn btn-danger">submit</button>

    </form>
</div>

<div class="p-6 bg-white border-b border-gray-200">                
    <h3><p><a href="{{ url('movies/addMovie') }}" class="btn btn-danger"> ADD MOVIE</a></p></h3>
</div>
<div class="p-6 bg-white border-b border-gray-200">                
    <h3><p><a href="{{ url('movies/editMovie') }}" class="btn btn-danger"> EDIT MOVIE</a></p></h3>
</div>
<div class="p-6 bg-white border-b border-gray-200">
         <h3><p><a href="/dashboard" class="btn btn-danger">Go to Dashboard</a></p></h3>
    </div>