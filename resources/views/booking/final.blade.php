
@extends('layouts.template')

<h2>Booking Successfull!!</h2>

<div>
<a href="{{ action('BookingController@cancelBooking') }}"class="btn btn-danger">Cancel Booking</a>

<a href="{{ action('MovieController@index') }}"class="btn btn-danger">return to home page</a>
</div>