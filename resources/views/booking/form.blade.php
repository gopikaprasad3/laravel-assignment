@extends('layouts.template')

<div style="margin: 10%;">
    <h1>book seats</h1>
    <form id="booking" method="post" action="{{ action('BookingController@addBooking') }}">
      <input  name="movie_id" value="{{ $booking->movie_id }}" hidden>
     

       <div class="form-group">
        <label for="email">list of chosen seats: </label>
        <?php if(isset($seats_number)) {
        foreach($seats_number as $seat) {
            echo $seat.', '; ?>
            <input type="hidden" name="seat[]" value="<?=$seat;?>"/>
        <?php } } ?>
      </div>
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <button type="submit" class="btn btn-danger">continue</button>
    </form>
</div>





//-------------------------------------------------------------------------------------------------


    public function deleteById($id){
        DB::beginTransaction();

        try {
            $movie = $this->movieRepository->delete($id);

        } catch (Exception $e) {
            DB::rollBack();
            Log::info($e->getMessage());

            throw new InvalidArgumentException('Unable to delete movie data');
        }

        DB::commit();

        return $movie;

    }

    
    public function getById($id)
    {
        return $this->movieRepository->getById($id);
    }

    /**
     * Update movie data
     * Store to DB if there are no errors.
     *
     * @param array $data
     * @return String
     */
    public function updateMovie($data, $id)
    {
        $validator = Validator::make($data, [
            'title' => 'bail|min:2',
            'description' => 'bail|max:255'
        ]);

        if ($validator->fails()) {
            throw new InvalidArgumentException($validator->errors()->first());
        }

        DB::beginTransaction();

        try {
            $post = $this->movieRepository->update($data, $id);

        } catch (Exception $e) {
            DB::rollBack();
            Log::info($e->getMessage());

            throw new InvalidArgumentException('Unable to update movie data');
        }

        DB::commit();

        return $movie;

    }

    /**
     * Validate movie data.
     * Store to DB if there are no errors.
     *
     * @param array $data
     * @return String
     */
    public function saveMovieData($data)
    {
        $validator = Validator::make($data, [
            'title' => 'required',
            'description' => 'required'
        ]);

        if ($validator->fails()) {
            throw new InvalidArgumentException($validator->errors()->first());
        }

        $result = $this->movieRepository->save($data);

        return $result;
    }

}