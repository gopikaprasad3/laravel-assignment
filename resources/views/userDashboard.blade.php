@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('Hello USER, You are logged in!') }}
                </div>
            </div>
        </div>
        <h2><p><a href="/movies">Click here for MOVIES LIST!!!</a></p></h2>

    </div>
</div>
@endsection