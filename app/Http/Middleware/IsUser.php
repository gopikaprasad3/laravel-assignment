<?php

namespace App\Http\Middleware;

use Closure;

class IsUser
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string  $role
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->user()->is_admin == 0) {
            return $next($request);
        }

        return redirect('/');

    }

}