<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request; 
use Illuminate\Support\Facades\Validator;
use App\Services\MovieService;


class MovieController extends Controller
{
    protected $movieService;

    public function __construct(MovieService $movieService){
		$this->movieService = $movieService;
	}

	public function dashboard(){
		if(Auth::user()->is_admin){
			return view('admin');
		}
		else{
			$movies = Movie::get();
			view('dashboard', ['movies'=>$movies]);
		}
	}
    
    public function index(){
		// $moviesList = Movie::all();
		$moviesList = $this->movieService->getAll();
		return view('movies.movieList', ['movies' => $moviesList] );

	}

	public function addMovie(Request $request){
		$validator = $request->validate([
			'title'=> 'required',
            'seats_available' => 'required',
            'show_time' => 'required',
            'description'=> 'required'
        ]);
		$movieName = $request->input();
		$addMovie=$this->movieService->addMovie($movieName);

		$moviesList = $this->movieService->getAll();
		return view('movies.movieList', ['movies' => $moviesList] );
      }

	public function addMovieView(){
		// $movies = $this->movieService->viewMovie();
		return view('movies.addmovie');


	}

	public function editMovie(Request $request){
		$validator = $request->validate([
			'title'=> 'required',
            'seats_available' => 'required',
            'show_time' => 'required',
            'description'=> 'required'
        ]);
		$movieName = $request->input();
		$editMovie=$this->movieService->editMovie($movieName,$request->$movie_id);


	}

	public function editMovieView(){
		return view('movies.editmovie');


	}
}
