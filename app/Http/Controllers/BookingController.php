<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request; 
use App\Services\BookingService;
use Illuminate\Support\Facades\Validator;


class BookingController extends Controller
{
    protected $bookingService;

    public function __construct(BookingService $bookingService){
		  $this->bookingService = $bookingService;
    }

    public function index(Request $request){
		  $movieDetails = $this->bookingService->getSeatNumbers($request->movie_id);
      return view('booking.addBooking', ['booking' => $movieDetails] );
    }


    public function list($id){
      $bookingList = $this->bookingService->getAll($id);
      return view('booking.addBooking', ['movie' => $bookingList] );
    }


    public function addBooking(Request $request){
      // dd(auth()->user()->id);

        $validator = $request->validate([
            'movie_id' => 'required',
            // 'user_id'  => 'required',
            'seats_number' => 'required'
        ]);
        // if ($validator->fails()) {
        //   throw new InvalidArgumentException($validator->errors()->first());
        // }
        $movie_id = $request->movie_id;
        $data = $request->input();
        $data['user_id'] = auth()->user()->id;
        $addBooking = $this->bookingService->addBooking($data);
        return view('booking.final');
     }


    public function viewBooking(){
        $movieDetails = $this->bookingService->getSeatNumbers($request->movie_id);
        return view('booking.addBooking', ['booking' => $movieDetails] );
     }


    public function cancelBooking(Request $request){
        $booking_id = $request->booking_id;
        $movie_id = $request->movie_id;
        $seats_number = $request->seats_number;
        $result = $this->bookingService->cancelBooking($booking_id,$seats_number,$movie_id,);
      return redirect('dashboard');
    }


}



