<?php

namespace App\Services;

use App\Models\Movie;
use App\Repositories\MovieRepository;
use App\Repositories\BookingRepository;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Validated;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use InvalidArgumentException;

class MovieService
{
    protected $movieRepository;
    protected $bookingRepository;

    public function __construct(MovieRepository $movieRepository,BookingRepository $bookingRepository){
        $this->movieRepository = $movieRepository;
        $this->bookingRepository = $bookingRepository;
    }

    public function getAll(){
        return $this->movieRepository->getAll();
    }

    public function addMovie($movieName){
        $result = $this->movieRepository->addMovie($movieName);
        return $result;
    }

    public function viewMovie(){
        return $this->movieRepository->viewMovie();
    }

    public function editMovie($movieName,$movie_id){
        return $this->movieRepository->editMovie($movieName,$movie_id);
    }

}