<?php

namespace App\Services;

use App\Models\Booking;
use App\Repositories\BookingRepository;
use App\Repositories\MovieRepository;

use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use InvalidArgumentException;


class BookingService
{
    
    protected $bookingRepository;
    protected $movieRepository;

    public function __construct(BookingRepository $bookingRepository, MovieRepository $movieRepository){
        $this->bookingRepository = $bookingRepository;
        $this->movieRepository = $movieRepository;
    }

    public function getSeatNumbers($movie_id){
        return $this->movieRepository->getMovieDetails($movie_id);
    }

    public function getAll($id){
        return $this->bookingRepository->getAll($id);
    }


    public function createSeat(){
        return $this->bookingRepository->createSeat();
    }


    public function addBooking($data){
        $result =  $this->bookingRepository->addBooking($data);
        $result =  $this->movieRepository->updateSeatNumbers($data);

        return $result;
    }
    

    public function cancelBooking(){
        DB::beginTransaction();
        try {
            $booking = $this->bookingRepository->cancelBooking($booking_id,$seats_number,$movie_id);
        }
        catch (Exception $e) {
            DB::rollBack();
            Log::info($e->getMessage());
            throw new InvalidArgumentException('Unable to delete booking data');
        }

        DB::commit();

        return $booking;

    }

   
     
    // public function updateMovie($data, $id)
    // {
    //     $validator = Validator::make($data, [
    //         'title' => 'bail|min:2',
    //         'description' => 'bail|max:255'
    //     ]);

    //     if ($validator->fails()) {
    //         throw new InvalidArgumentException($validator->errors()->first());
    //     }

    //     DB::beginTransaction();

    //     try {
    //         $post = $this->movieRepository->update($data, $id);

    //     } catch (Exception $e) {
    //         DB::rollBack();
    //         Log::info($e->getMessage());

    //         throw new InvalidArgumentException('Unable to update movie data');
    //     }

    //     DB::commit();

    //     return $movie;

    // }

  
//     public function saveBookingData($data)
//     {
//         $validator = Validator::make($data, [
//             'title' => 'required',
//             'description' => 'required'
//         ]);

//         if ($validator->fails()) {
//             throw new InvalidArgumentException($validator->errors()->first());
//         }

//         $result = $this->movieRepository->save($data);

//         return $result;
//     }

 }