<?php
namespace App\Repositories;
use App\Models\Booking;
use App\Models\Movie;


class BookingRepository
{
   
    public function getSeatNumbers($movie_id){
        return Booking::where('movie_id', $movie_id)->pluck('seats_number');
    }


// public  function getAll()
// {
//     return Booking::select('booking_id','movie_id','seats_number')->get();
// }

    public  function getAll($id){
        $result= Movie::where('movie_id',$id)->get()->first();
        // dd($result);
        return $result;
    }


    public function addBooking($data){
        // $movie_id = $request->input('movie');
        $booking = new Booking;
        $booking->movie_id = $data['movie_id'];
        $booking->seats_number = $data['seats_number'];
        $booking->user_id = $data['user_id'];
        $booking->save();
        return  $booking;
    }
     
    public function cancelBooking($booking_id,$seats_number,$movie_id){
        $movie = $this->movie->find($movie_id);
        $movie->seats_available += $seats_number;
        $movie->update();

        $booking = $this->booking->find($booking_id);
        $booking->delete();
        return true;
    }
     
} 
    
