<?php
namespace App\Repositories;
use App\Models\Movie;
use App\Models\Booking;
use Illuminate\Support\Facades\DB;

class MovieRepository
{
    protected $movie;

    public function __construct(Movie $movie,Booking $booking){
        $this->movie = $movie;
    }
    

    public function addMovie($movieName){
        $movie = new Movie;
        $movie->title = $movieName['title'];
        $movie->description = $movieName['description'];
        $movie->seats_available = $movieName['seats_available'];
        $movie->show_time = $movieName['show_time'];
        $movie->save();
        return $movie;
    }

    public function updateSeatNumbers($data){
        $movie = $this->movie->find($data['movie_id']);
        $movie->seats_available -= $data['seats_number'];
        $movie->update();
        return $movie;
    }

    public  function getAll(){
        return Movie::select('title','description','movie_id')->get();
    }

    public function getMovieDetails($movie_id){
        return Movie::where('movie_id','=',$movie_id)->get();
    }

    
    public function editMovie($movieName,$movie_id){
        $movie = $this->movie->find($movie_id);
        $movie->title = $movieName['title'];
        $movie->description = $movieName['description'];
        $movie->seats_available = $movieName['seats_available'];
        $movie->show_time = $movieName['show_time'];
        $movie->update();
        return $movie;
    }

    public function delete($id){
    return Movie::delete($id);
    }


}