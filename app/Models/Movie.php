<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
    use HasFactory;



    protected $table = 'movies';
	public $timestamps = true;
    protected $primaryKey = 'movie_id';
    protected $fillable = [
        'title',
        'seats_available',
        'show_time',
        'description'
        
    ];
}
