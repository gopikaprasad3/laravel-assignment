<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class moviesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('movies')->insert([
            'title' => 'Chakra',
            'seats_available' => '120',
            'show_time' => '18:00:00',
            'description' =>  'Chakra is a cyber-crime thriller where an officer is set on a mission to take down a bunch 
            of goons who practise evil acts on the internet.'    
            
        ]);

        DB::table('movies')->insert([
            'title' => 'Wonder Women',
            'seats_available' => '100',
            'show_time' => '15:00:00',
            'description' =>  'Set in the 1980s, Wonder Woman`s next big screen adventure finds her facing two all-new foes, 
            Max Lord and The Cheetah, and the unexpected return of a face from her past.'    
            
        ]);
        
    }
}
