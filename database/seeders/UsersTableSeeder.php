<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'gopika',
            'email' => 'gopika@gmail.com',
            'password' => bcrypt('gopika'),
            'is_admin' => 1         
            

        ]);
        DB::table('users')->insert([
            'name' => 'diya',
            'email' => 'diya@gmail.com',
            'password' => bcrypt('diya'),
            'is_admin' => 0  
        ]);
    }
}
