<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MovieController;
use App\Http\Controllers\BookingController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';


// Route::get('/movies', 'MovieController@index')->middleware(['auth']);

    Route::middleware(['auth'])->group(function(){
    Route::get('/movies', 'MovieController@index');

    Route::get('/booking/index/{movie_id?}', 'BookingController@index');
    Route::get('/booking/list/{id?}', 'BookingController@list');
    
     Route::get('/booking/addBooking', 'BookingController@viewBooking');
    Route::post('/booking/addBooking', 'BookingController@addBooking');
    
    Route::post('/booking/cancel', 'BookingController@cancelBooking');
    
   

    });


    Route::middleware(['auth','is_admin'])->group(function(){

    // Route::get('/movies', 'MovieController@index');
    Route::post('/movies/addMovie', 'MovieController@addMovie');
    Route::get('/movies/addMovie', 'MovieController@addMovieView');

    Route::post('/movies/editMovie', 'MovieController@editMovie');
    Route::get('/movies/editMovie', 'MovieController@addMovieView');
    
    // Route::get('/booking/index/{movie_id?}', 'BookingController@index');
    // Route::get('/booking/list/{id?}', 'BookingController@list');
    
    //  Route::get('/booking/addBooking', 'BookingController@viewBooking');
    // Route::post('/booking/addBooking', 'BookingController@addBooking');
    
    // Route::post('/booking/cancel', 'BookingController@cancelBooking');
    
    
   });

// --------------------------------------------------------------------------------------------
// Route::middleware(['auth','is_user'])->group(function(){
// Route::get('/movies', 'MovieController@index');
// });
// Route::post('/movies/addMovie', 'MovieController@addMovie');
// Route::get('/movies/addMovie', 'MovieController@addMovieView');


// Route::post('/movies/editMovie/{movie_id?}', 'MovieController@editMovie');
// Route::get('/movies/editMovie', 'MovieController@editMovieView');



// Route::get('/booking/index/{movie_id?}', 'BookingController@index');
// Route::get('/booking/list/{id?}', 'BookingController@list');

// // Route::get('/booking/addBooking', 'BookingController@viewBooking');ss
// Route::post('/booking/addBooking', 'BookingController@addBooking');

// Route::post('/booking/cancel', 'BookingController@cancelBooking');






